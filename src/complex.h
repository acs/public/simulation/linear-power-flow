#ifndef COMPLEXTYPE

#define COMPLEXTYPE
using namespace std;

class Complex {
public:
	double r;
	double i;
public:
	Complex();
	Complex(double, double);
	Complex(double);
	Complex operator+(Complex op2);
	Complex operator-(Complex op2);
	Complex operator*(Complex op2);
	Complex operator/(Complex op2);
	void operator+=(Complex op2);
	void operator-=(Complex op2);
	Complex Conj();
	double abs();
	double arg();

	void print();
};

#endif 