#ifndef COMPLEXPROBLEM

#define COMPLEXPROBLEM

#include <iostream>
#include "complex.h"
#include "Matrix.h"
#include "ComplexMatrix.h"

using namespace std;

class ComplexProblem {
public:
	int n;
	
	ComplexProblem() {};
	ComplexProblem(int size);
	void ExpandMatrixA();
	void ExpandMatrixB();
	ComplexMatrix Solve();
	void CompressSolution();
	void PrepareSolution();
	void SetSize(int size);
	
	ComplexMatrix A;
	ComplexMatrix B;
	ComplexMatrix x;
	Matrix Aex;
	Matrix Bex;
	Matrix xex;
	LU luFactored;

};

#endif
#pragma once
