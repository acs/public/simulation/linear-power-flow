#ifndef IMPEDANCE_H
#define IMPEDANCE_H
#include "CircuitElement.h"
#include "Matrix.h"
#include "ComplexMatrix.h"
#include <iostream>
using std::cout;
using std::endl;

class Impedance : public CircuitElement {
	protected:
		double inductance;
		double resistance;
		Complex Z;
		Complex G;

	public:
		Impedance() {;};
		Impedance(std::string name, int src, int dest, double R, double X);
		void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B);
		void Init(ComplexMatrix& A, ComplexMatrix& B);
		void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);
	};
#endif