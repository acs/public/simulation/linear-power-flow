#include "stdafx.h"
#include "ConstantPowerLoad.h"

ConstantPowerLoad::ConstantPowerLoad(std::string name, int src, int dest, double active, double reactive) {
			node1 = src;
			node2 = dest;
			ActivePower = active;
			ReactivePower = reactive;
			ApparentPower = Complex(ActivePower, ReactivePower);
		};
		
		
		

void ConstantPowerLoad::applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) {
					};
	
void ConstantPowerLoad::Init(ComplexMatrix& A, ComplexMatrix& B){
applyMatrixStamp(A,B);
}

void ConstantPowerLoad::Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol){
	Complex Vcur, Cur;
	Complex Vpos, Vneg;
	Complex MOne(-1, 0);
	if (node1 > 0)
		Vpos = sol.get(node1, 1);
	if (node2 > 0)
		Vneg = sol.get(node2, 1);
	Vcur = Vpos - Vneg;
	Cur = ApparentPower / Vcur;
	if (node1 > 0)
		B.set(node1, 1, Cur);

	if (node2 > 0) {
		Cur = MOne * Cur;
		B.set(node2, 1, Cur);
	}
		
}
