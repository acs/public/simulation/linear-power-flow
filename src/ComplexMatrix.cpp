#include "stdafx.h"
#include "ComplexMatrix.h"


ComplexMatrix::ComplexMatrix(int rows, int columns) {
	numRows = rows;
	numColumns = columns;
	for (int i = 0; i < numRows; i++) {
		VectorJI<Complex> newVec;
		newVec.setSize(numColumns);
		rowVec.add(newVec);
	}
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			this->set(i,j,0);
		}
	}
}


void ComplexMatrix::SetSize(int rows, int columns) {
	numRows = rows;
	numColumns = columns;
	for (int i = 0; i < numRows; i++) {
		VectorJI<Complex> newVec;
		newVec.setSize(numColumns);
		rowVec.add(newVec);
	}
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			this->set(i,j,0);
		}
	}
}



void ComplexMatrix::clear() {
	for(int i=1; i<=numRows; i++)
		for(int j=1; j<=numColumns; j++)
			set(i,j,0.0);
}


int ComplexMatrix::getNumRows() const {
	return numRows;
}

int ComplexMatrix::getNumColumns() const {
	return numColumns;
}

Complex ComplexMatrix::get(int row, int column) const {
	return ((VectorJI<Complex>)(rowVec.elementAt(row-1))).elementAt(column-1);
}

double ComplexMatrix::get(int row, int column, int part) const {
	if(part == REAL)
		return ((VectorJI<Complex>)(rowVec.elementAt(row - 1))).elementAt(column - 1).r;
	else
		return ((VectorJI<Complex>)(rowVec.elementAt(row - 1))).elementAt(column - 1).i;
}


void ComplexMatrix::set(int row, int column, Complex value) {
	VectorJI<Complex> old = rowVec.elementAt(row - 1);
	old.setElementAt(value, column - 1);
	rowVec.setElementAt(old, row - 1);
}


void ComplexMatrix::printMatrix() const {
	Complex a;
	for (int j = 1; j <= numRows; j++) {
		a = get(j, 1);
		cout << "["; 
		a.print();
		for (int i=2; i <= numColumns; i++) {
			a = get(j, i);
			cout << "\t";
			a.print();
		}
		cout << "]\n";
	}
	cout << endl;
}

double ComplexMatrix::norm() {
	double Norm = 0;
	if (numRows == 1) {
		for (int i = 1; i <= numColumns; i++) {
			double re, im;
			re = get(1, i).r;
			im = get(1, i).i;
			Norm += re * re + im * im;
		}
	}
	else if (numColumns == 1) {
		for (int i = 1; i <= numRows; i++) {
			double re, im;
			re = get(i, 1).r;
			im = get(i, 1).i;
			Norm += re * re + im * im;
		}

	}
	return(Norm);
}

ComplexMatrix ComplexMatrix::operator +(ComplexMatrix op2) {
	ComplexMatrix a(numRows,numColumns);
	Complex s1, s2;
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			s1 = get(i, j);
			s2 = op2.get(i, j);
			a.set(i, j, s1 + s2);
		}
	}
	return a;
}

ComplexMatrix ComplexMatrix::operator -(ComplexMatrix op2) {
	ComplexMatrix a(numRows, numColumns);;
	Complex s1, s2;
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			s1 = get(i, j);
			s2 = op2.get(i, j);
			a.set(i, j, s1 - s2);
		}
	}
	return a;
}

