#include "stdafx.h"
#include "Impedance.h"


Impedance::Impedance(std::string name, int src, int dest, double R, double X) {
			Complex One(1, 0);
			this->resistance = R;
			this->inductance = X;
			Z = Complex(R, X);
			G = One/ Z;

			node1 = src;
			node2 = dest;
		};

	
		
void Impedance::applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) {

			if (node1 != 0)
			{
			A.set(node1,node1, A.get(node1,node1)+ G);
			
			if  (node2 != 0)
			{
			A.set(node1,node2, A.get(node1,node2) - G);
			A.set(node2,node1, A.get(node2,node1) - G);
			A.set(node2,node2, A.get(node2,node2)+ G);
			}
			}
			else if (node2 != 0)
			{
			A.set(node2,node2, A.get(node2,node2)+ G);
			}
		};
		
void Impedance::Init(ComplexMatrix& A, ComplexMatrix& B){
applyMatrixStamp(A,B);
}


void Impedance::Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol){
}


