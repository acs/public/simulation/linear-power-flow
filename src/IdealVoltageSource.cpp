#include "stdafx.h"
#include "IdealVoltageSource.h"

IdealVoltageSource::IdealVoltageSource(std::string name, int src, int dest, double voltage, double phase) {
			this->SourceVoltage = Complex(voltage*cos(phase), voltage*sin(phase));
			node1 = src;
			node2 = dest;
		};
		
		
		

void IdealVoltageSource::applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) {
			
			Complex COne(1,0);
			Complex NOne(-1, 0);
			
			internalNodeNum = B.getNumRows();
	
			
			if (node1 != 0)
			{
				A.set(node1, internalNodeNum, COne);
				A.set(internalNodeNum, node1, COne);
			}
			if  (node2 != 0)
			{
				A.set(node2, internalNodeNum, NOne);
				A.set(internalNodeNum, node2, NOne);

			}
			
		};
	
void IdealVoltageSource::Init(ComplexMatrix& A, ComplexMatrix& B){
applyMatrixStamp(A,B);
}

void IdealVoltageSource::Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol){

	B.set(internalNodeNum, 1, SourceVoltage);
}
