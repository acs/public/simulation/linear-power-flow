#include "stdafx.h"
#include "Matrix.h"

Matrix LU::extractUpperTriangularMatrix() const {
	Matrix ut(Q.getNumRows(), Q.getNumColumns());
	ut.setAsIdentityMatrix();
	for (int row = 1; row <= Q.getNumRows(); row++) {
		for (int column = row+1; column <= Q.getNumColumns(); column++) {
			ut.set(row, column, Q.get(row,column));
		}
	}
	return ut;
}

Matrix LU::extractLowerTriangularMatrix() const {
	Matrix lt(Q.getNumRows(), Q.getNumColumns());
	lt.setAsIdentityMatrix();
	for (int column = 1; column <= Q.getNumColumns(); column++) {
		for (int row = column; row <= Q.getNumRows(); row++) {
			lt.set(row, column, Q.get(row,column));
		}
	}
	return lt;
}

bool Matrix::isEqual(const Matrix& b) const {
	if (numRows != b.numRows || numColumns != b.numColumns) {
		return false;
	}
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			if (get(i,j) != b.get(i,j)) {
				return false;
			}
		}
	}
	return true;
}

Matrix::Matrix(int rows, int columns) {
	numRows = rows;
	numColumns = columns;
	for (int i = 0; i < numRows; i++) {
		VectorJI<double> newVec;
		newVec.setSize(numColumns);
		rowVec.add(newVec);
	}
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			this->set(i,j,0);
		}
	}
}


void Matrix::SetSize(int rows, int columns) {
	numRows = rows;
	numColumns = columns;
	for (int i = 0; i < numRows; i++) {
		VectorJI<double> newVec;
		newVec.setSize(numColumns);
		rowVec.add(newVec);
	}
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			this->set(i,j,0);
		}
	}
}


void Matrix::addRow() {
	addRow(Matrix(1,numColumns));
}

void Matrix::addColumn() {
	addColumn(Matrix(numRows,1));
}

void Matrix::clear() {
	for(int i=1; i<=numRows; i++)
		for(int j=1; j<=numColumns; j++)
			set(i,j,0.0);
}


void Matrix::removeColumn(int column) {
	for (int i = 0; i < numRows; i++) {
		VectorJI<double> old = rowVec.elementAt(i);
		old.remove(column - 1);
		rowVec.setElementAt(old, i);
		//(rowVec.elementAt(i)).remove(column - 1);
	}
	numColumns --;
}

void Matrix::removeRow(int row) {
	rowVec.remove(row - 1);
	numRows--;
}

void Matrix::addRow(const Matrix& row) {
	VectorJI<double> rowval = row.rowVec.elementAt(0);
	rowVec.add(numRows, rowval);
	numRows++;
}

void Matrix::addColumn(const Matrix& column) {
	for (int i = 0; i < numRows; i++) {
		VectorJI<double> old = rowVec.elementAt(i);
		old.add(column.get(i+1,1));
		rowVec.setElementAt(old, i);
//		((VectorJI<double>)rowVec.elementAt(i)).add(column.get(i+1,1));
	}
	numColumns++;
}

int Matrix::getNumRows() const {
	return numRows;
}

int Matrix::getNumColumns() const {
	return numColumns;
}

double Matrix::get(int row, int column) const {
	return ((VectorJI<double>)(rowVec.elementAt(row-1))).elementAt(column-1);
}

void Matrix::set(int row, int column, double value) {
	/*((VectorJI<double>)(rowVec.elementAt(row-1))).setElementAt(value, column-1);*/
	VectorJI<double> old = rowVec.elementAt(row - 1);
	old.setElementAt(value, column - 1);
	rowVec.setElementAt(old, row - 1);
}

void Matrix::setAsIdentityMatrix() {
	if (numRows != numColumns) {
		cout << "Invalid dimensions in matrix calling Matrix.setAsIdentityMatrix() - matrix must be square." << endl;
		return;
	}
	
	for (int i = 1; i <= numRows; i++) {
		for (int j = 1; j <= numColumns; j++) {
			if (i == j) {
				this->set(i,j,1);
			} else {
				this->set(i,j,0);
			}
		}
	}
}

Matrix Matrix::transposeMatrix(const Matrix& input) {
	Matrix transpose(input.getNumColumns(), input.getNumRows());

	for (int i = 1; i <= transpose.numRows; i++) {
		for (int j = 1; j <= transpose.numColumns; j++) {
			transpose.set(i,j,input.get(j,i));
		}
	}
	
	return transpose;
}

Matrix Matrix::augmentMatrix(const Matrix& matrix1, const Matrix& matrix2) {
	if (matrix1.getNumRows() != matrix2.getNumRows()) {
		cout << "Error: attempting to augment two matrices with different numbers of rows." << endl;
		return Matrix(0,0);
	}
	
	Matrix augmented(matrix1.getNumRows(), matrix1.getNumColumns() + matrix2.getNumColumns());
	
	for (int i = 1; i <= matrix1.numRows; i++) {
		for (int j = 1; j <= matrix1.numColumns; j++) {
			augmented.set(i,j,matrix1.get(i,j));
		}
	}
	for (int i = 1; i <= matrix2.numRows; i++) {
		for (int j = 1; j <= matrix2.numColumns; j++) {
			augmented.set(i,j+matrix1.numColumns,matrix2.get(i,j));
		}
	}
	
	return augmented;
}

void Matrix::interchangeRows(int row1, int row2) {
	VectorJI<double> temp = rowVec.get(row1-1);
	rowVec.setElementAt(rowVec.get(row2-1), row1-1);
	rowVec.setElementAt(temp, row2-1);
}

Matrix Matrix::addMatrices(const Matrix& addend1, const Matrix& addend2) {
	if (addend1.numColumns != addend2.numColumns || addend1.numRows != addend2.numRows) {
		cout << "Error: Cannot add matrices with incompatible dimensions." << endl;
		return Matrix(0,0);
	}
	Matrix sum(addend1.numRows, addend1.numColumns);
	for (int i = 1; i <= sum.numRows; i++) {
		for (int j = 1; j <= sum.numColumns; j++) {
			sum.set(i,j,addend1.get(i,j)+addend2.get(i,j));
		}
	}
	return sum;
}

Matrix Matrix::multiplyByScalar(const Matrix& matrix, double factor) {
	Matrix scaledMat(matrix.getNumRows(), matrix.getNumColumns());
	for (int row =1; row<=matrix.getNumRows(); row++) {
		for (int col = 1; col <= matrix.getNumColumns(); col++) {
			scaledMat.set(row,col,factor*matrix.get(row,col));
		}
	}
	return scaledMat;
}

Matrix Matrix::subtractMatrices(const Matrix& a, const Matrix& b) {
	if (a.numColumns != b.numColumns || a.numRows != b.numRows) {
		cout << "Error: Cannot subtract matrices with incompatible dimensions." << endl;
		return Matrix(0,0);
	}
	Matrix diff(a.numRows, a.numColumns);
	for (int i = 1; i <= diff.numRows; i++) {
		for (int j = 1; j <= diff.numColumns; j++) {
			diff.set(i,j,a.get(i,j)-b.get(i,j));
		}
	}
	return diff;
}

Matrix Matrix::multiplyMatrices(const Matrix& m1, const Matrix& m2) {
	if (m1.numColumns != m2.numRows) {
		cout << "Incompatible matrices requested for multiplication." << endl;
		return Matrix(0,0);
	}
	
	Matrix product(m1.numRows, m2.numColumns);
	for (int row = 1; row <= product.numRows; row++) {
		for (int col = 1; col <= product.numColumns; col++) {
			double sum = 0;
			for (int n = 1; n <= m1.numColumns; n++) {
				sum += m1.get(row,n)*m2.get(n,col);
			}
			product.set(row,col,sum);
		}
	}
	
	return product;
}

void Matrix::setRow(int row, double* values) {
	for (int i = 1; i <= numColumns; i++) {
		this->set(row,i,values[i-1]);
	}
}

Matrix Matrix::extractColumn(int column) const {
	Matrix columnMat(numRows,1);
	for (int i = 1; i <= numRows; i++) {
		columnMat.set(i,1,get(i,column));
	}
	return columnMat;
}

void Matrix::replaceColumn(int column, const Matrix& colMat) {
	for (int i = 1; i <= numRows; i++) {
		this->set(i,column,colMat.get(i,1));
	}
}

Matrix Matrix::extractRow(int row) const {
	VectorJI<double> rowExt = rowVec.get(row-1);
	Matrix rowMat(1, numColumns);
	rowMat.rowVec.setElementAt(rowExt, 0);
	return rowMat;
}

void Matrix::replaceRow(int row, const Matrix& rowMat) {
	this->rowVec.setElementAt(rowMat.rowVec.get(0), row - 1);
}

void Matrix::printMatrix() const {
	for (int j = 1; j <= numRows; j++) {
		cout << "[" << get(j,1);
		for (int i=2; i <= numColumns; i++) {
			cout << "\t" << get(j,i);
		}
		cout << "]\n";
	}
	cout << endl;
}

Matrix Matrix::computeInverse(const Matrix& A, const LU& luFactored) {
	Matrix identity (A.numRows, A.numColumns);
	identity.setAsIdentityMatrix();
	Matrix inverse(A.numRows, A.numColumns);
	for (int col = 1; col <= A.numColumns; col++) {
		inverse.replaceColumn(col,Matrix::solveLinearSystem(A,identity.extractColumn(col),luFactored));
	}
	return inverse;
}

Matrix Matrix::solveLinearSystem(const Matrix& A, const Matrix& b, const LU& luFactor) {
	Matrix d = Matrix::multiplyMatrices(luFactor.rowPermutationMat,b);
	Matrix y(A.numRows,1);
	for (int k = 1; k <= A.numRows; k++) {
		double prodsum = 0;
		for (int i =1; i<=k-1; i++) {
			prodsum += luFactor.Q.get(k,i)*y.get(i,1);
		}
		y.set(k,1,(d.get(k,1)-prodsum)/luFactor.Q.get(k,k));
	}
	Matrix x(A.numRows,1);
	for (int k = A.numRows; k >= 1; k--) {
		double prodsum = 0;
		for (int i=k+1; i <= A.numRows; i++) {
			prodsum += luFactor.Q.get(k,i)*x.get(i,1);
		}
		x.set(k,1, y.get(k,1) - prodsum);
	}
	return x;
}

//returns RHS of partition, original matrix now contains LHS
Matrix Matrix::partitionMatrix(int afterWhichColumn) {
	Matrix RHS(numRows, numColumns - afterWhichColumn);
	for (int row = 1; row <= RHS.getNumRows(); row++) {
		for (int col=1; col <= RHS.getNumColumns(); col++) {
			RHS.set(row,col,get(row, afterWhichColumn+col));
		}
		VectorJI<double> old = rowVec.elementAt(row-1);
		old.setSize(afterWhichColumn);
		rowVec.setElementAt(old, row - 1);
		//((VectorJI<double>)(rowVec.elementAt(row-1))).setSize(afterWhichColumn);
	}
	numColumns = afterWhichColumn;
	return RHS;
}

LU Matrix::computeLUFactorization() const {
	Matrix identity(numRows, numColumns);
	identity.setAsIdentityMatrix();
	Matrix augmented = Matrix::augmentMatrix(*this,identity);
	double det = 1;
	for (int j =1; j <= numRows; j++) {
		for (int k = j; k <= numRows; k++) {
			double prodsum = 0;
			for (int i=1; i <=j-1; i++) {
				prodsum += augmented.get(k,i) * augmented.get(i,j);
			}
			augmented.set(k,j,augmented.get(k,j)-prodsum);
		}
		
		int rowPivot=j;
		double currMax = abs(augmented.get(j,j));
		for (int i = j; i <= numRows; i++) {
			if (abs(augmented.get(i,j)) > currMax) {
				currMax = abs(augmented.get(i,j));
				rowPivot = i;
			}
		}
		if (currMax == 0) {
			cout << "Singular matrix - cannot compute LU factorization." << endl;
			return LU();
		}
		if (rowPivot > j) {
			augmented.interchangeRows(rowPivot,j);
			det *= -1;
		}
		
		for (int k = j+1; k <= numRows; k++) {
			double prodsum = 0;
			for (int i =1; i <=j-1; i++) {
				prodsum += augmented.get(j,i)*augmented.get(i,k);
			}
			double newVal = (augmented.get(j,k) - prodsum)/augmented.get(j,j);
			augmented.set(j,k,newVal);
		}
		det *= augmented.get(j,j);			
	}
	LU returnLU;
	returnLU.rowPermutationMat = augmented.partitionMatrix(numRows);
	returnLU.Q = augmented;
	returnLU.determinant = det;
	return returnLU;
}