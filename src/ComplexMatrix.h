#ifndef COMPMATRIX_H
#define COMPMATRIX_H

#include "VectorJI.h"
#include <iostream>
#include <math.h>
#include "complex.h"

using std::cout;
using std::endl;

#define REAL 0
#define IMAG 1

class ComplexMatrix;

class ComplexMatrix 
{
	public:	
		ComplexMatrix() { numRows = numColumns = 0; };
		ComplexMatrix(int rows, int columns);
		void SetSize(int rows, int columns);
		int getNumRows() const;		
		int getNumColumns() const;	
		Complex get(int row, int column) const;	
		double get(int row, int column, int part) const;

		void set(int row, int column, Complex value);		
		void clear();
		void printMatrix() const;	
		ComplexMatrix operator +(ComplexMatrix op2);
		ComplexMatrix operator -(ComplexMatrix op2);
		double norm();

	private:
		int numRows;
		int numColumns;
		VectorJI<VectorJI<Complex>> rowVec;
};
#endif