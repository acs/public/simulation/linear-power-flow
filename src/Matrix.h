#ifndef MATRIX_H
#define MATRIX_H

//#include "LU.h"
#include "VectorJI.h"
#include <iostream>
#include <math.h>

using std::cout;
using std::endl;

class LU;
class Matrix;

class Matrix 
{
	public:	
		Matrix() { numRows = numColumns = 0; };
		Matrix(int rows, int columns);
		void SetSize(int rows, int columns);
		bool isEqual(const Matrix& b) const;
		void removeColumn(int column);		
		void removeRow(int row);
		void addRow();
		void addColumn();
		void addRow(const Matrix& row);
		void addColumn(const Matrix& column);		
		int getNumRows() const;		
		int getNumColumns() const;	
		double get(int row, int column) const;		
		void set(int row, int column, double value);		
		void clear();
		void setAsIdentityMatrix();		
		static Matrix transposeMatrix(const Matrix& input);		
		static Matrix augmentMatrix(const Matrix& matrix1, const Matrix& matrix2);		
		void interchangeRows(int row1, int row2);
		static Matrix addMatrices(const Matrix& addend1, const Matrix& addend2);		
		static Matrix multiplyByScalar(const Matrix& matrix, double factor);		
		static Matrix subtractMatrices(const Matrix& a, const Matrix& b);		
		static Matrix multiplyMatrices(const Matrix& m1, const Matrix& m2);		
		void setRow(int row, double* values);		
		Matrix extractColumn(int column) const;	
		void replaceColumn(int column, const Matrix& colMat);		
		Matrix extractRow(int row) const;
		void replaceRow(int row, const Matrix& rowMat);		
		void printMatrix() const;		
		static Matrix computeInverse(const Matrix& A, const LU& luFactored);		
		static Matrix solveLinearSystem(const Matrix& A, const Matrix& b, const LU& luFactor);		
		Matrix partitionMatrix(int afterWhichColumn); //returns RHS of partition, original matrix now contains LHS
		LU computeLUFactorization() const;
	private:
		int numRows;
		int numColumns;
		VectorJI<VectorJI<double>> rowVec;
};

class LU 
{
	public:
		LU() { Q = rowPermutationMat = Matrix();};
		Matrix extractUpperTriangularMatrix() const;
		Matrix extractLowerTriangularMatrix() const;

		Matrix Q;
		Matrix rowPermutationMat;
		double determinant;
};

	
#endif