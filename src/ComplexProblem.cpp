#include "stdafx.h"
#include "ComplexProblem.h"


ComplexProblem::ComplexProblem(int size) {
	n = size;
	Aex.SetSize(2 * size, 2 * size);
	Bex.SetSize(2 * size, 1);
	xex.SetSize(2 * size, 1);
	A.SetSize(size,size);
	B.SetSize(size,1);
	x.SetSize(size,1);
}

void ComplexProblem::SetSize(int size) {
	n = size;
	Aex.SetSize(2 * size, 2 * size);
	Bex.SetSize(2 * size, 1);
	xex.SetSize(2 * size, 1);
	A.SetSize(size, size);
	B.SetSize(size, 1);
	x.SetSize(size, 1);
}


void ComplexProblem::PrepareSolution() {
	ExpandMatrixA();
	luFactored = Aex.computeLUFactorization();
}

ComplexMatrix ComplexProblem::Solve() {
	ExpandMatrixB();
	xex = Matrix::solveLinearSystem(Aex, Bex, luFactored);
	CompressSolution();
	return(x);
}

void ComplexProblem::ExpandMatrixA() {
	for (int i = 1; i <= n; i++) 
		for (int j = 1; j <= n; j++) {
			Aex.set(i,j, A.get(i,j,REAL));
			Aex.set(i+n,j+n, A.get(i,j, REAL));
			Aex.set(i,j+n, -A.get(i,j,IMAG));
			Aex.set(i+n,j, A.get(i,j,IMAG));
		}
	}

 void ComplexProblem::ExpandMatrixB() {
	for (int i = 1; i <= n; i++) {
			Bex.set(i,1,B.get(i,1,REAL));
			Bex.set(i + n, 1, B.get(i,1,IMAG));
		}
	}

void ComplexProblem::CompressSolution() {
	Complex J(0, 1);
	double re, im;
	Complex a;
	for (int i = 1; i <= n; i++) {
		re = xex.get(i, 1);
		im = xex.get(i + n, 1);
		a = Complex(re, im);
		x.set(i,1,a);
	}
}


