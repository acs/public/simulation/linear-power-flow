#include "stdafx.h"
#include "NonIdealSource.h"

NonIdealSource::NonIdealSource(std::string name, int src, int dest, double vnom, double Pnom, double R, double X, double alfa) {
			node1 = src;
			node2 = dest;
			ActivePower = Pnom;
			NominalVoltage = vnom;
			InternalImpedance = Complex(R, X);
			this->alfa = alfa;
			Complex One(1, 0);
			G = One / InternalImpedance;
		};
		
		
		

void NonIdealSource::applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) {
	if (node1 != 0)
	{
		A.set(node1, node1, A.get(node1, node1) + G);

		if (node2 != 0)
		{
			A.set(node1, node2, A.get(node1, node2) - G);
			A.set(node2, node1, A.get(node2, node1) - G);
			A.set(node2, node2, A.get(node2, node2) + G);
		}
	}
	else if (node2 != 0)
	{
		A.set(node2, node2, A.get(node2, node2) + G);
	}
					};
	
void NonIdealSource::Init(ComplexMatrix& A, ComplexMatrix& B){
applyMatrixStamp(A,B);
Vcur = Complex(1, 0);
Vold = Complex(1, 0);
delta = 0;
}

void NonIdealSource::Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol){
	Complex Cur;
	Complex Vpos, Vneg;
	Complex MOne(-1, 0);
	Complex Acur;
	Complex diff;
	double angle;

	if (node1 > 0)
		Vpos = sol.get(node1, 1);
	if (node2 > 0)
		Vneg = sol.get(node2, 1);
	Vcur = Vpos - Vneg;
	angle = delta + atan2(Vcur.i, Vcur.r);
	Complex Vtheo(NominalVoltage*cos(angle), NominalVoltage*sin(angle));
	diff = Vtheo - Vcur;
	Acur = Vcur * diff.Conj()*G;	
	
	double dP;
	dP = (ActivePower - Acur.r);

	diff = Vcur - Vold;

	double t1, td;
	t1 = NominalVoltage*diff.abs()/InternalImpedance.abs()*sin(delta);
	td = NominalVoltage * Vcur.abs() / InternalImpedance.abs()*cos(delta);

	if (abs(td) > 1e-10)
		delta += alfa * (dP - t1) / td;


	angle = delta + atan2(Vcur.r,Vcur.i);
	Complex Vsource(NominalVoltage*cos(angle), NominalVoltage*sin(angle));
	
	Vold = Vcur;



	Cur = Vsource / InternalImpedance;
	if (node1 > 0)
		B.set(node1, 1, Cur);

	if (node2 > 0) {
		Cur = MOne * Cur;
		B.set(node2, 1, Cur);
	}
		
}
