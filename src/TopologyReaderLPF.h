#ifndef TOPOLOGYREADER_H
#define TOPOLOGYREADER_H

#include <fstream>
#include <vector>
#include <string>

#include "LPF_Sim.h"
#include "CircuitElement.h"

typedef struct { 
	std::string type;
	std::string name; 	
	std::string node1;
	std::string node2;
	std::vector<std::string> parameters; 
} Element; 

typedef struct {
	std::string MaxIter;
	std::string Toll;
	std::string Alfa;
	std::vector<Element> elements;
} Config;


class TopologyReaderLPF {
	private:

	public:
		TopologyReaderLPF();
		~TopologyReaderLPF();
		int readConfig(std::ifstream &f, Config &conf);
		int parseConfig(Config &conf, vector<CircuitElement*> &circElements, int &MaxIter, double &Toll, double& Alfa);
};




#endif
