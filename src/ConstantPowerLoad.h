#ifndef CONSTANTPOWERLOAD_H
#define CONSTANTPOWERLOAD_H
#include "CircuitElement.h"
#include "Matrix.h"
#include "ComplexMatrix.h"

#include <iostream>
using std::cout;
using std::endl;

class ConstantPowerLoad : public CircuitElement {
	protected:
		double ActivePower;
		double ReactivePower;
		Complex ApparentPower;
		
	public:
		ConstantPowerLoad() {;};
		ConstantPowerLoad(std::string name, int src, int dest, double active, double reactive);
		
		void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) ;
		void Init(ComplexMatrix& A, ComplexMatrix& B);		
		void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);

};
#endif