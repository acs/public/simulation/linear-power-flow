#include "stdafx.h"
#include "PILine.h"


PILine::PILine(std::string name, int src, int dest, double R, double X, double Xc) {
			Complex One(1, 0);
			this->resistance = R;
			this->inductance = X;
			Z = Complex(R, X);
			G = One/ Z;
			Gc = One / Xc;
			node1 = src;
			node2 = dest;
		};

	
		
void PILine::applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) {

			if (node1 != 0)
			{
			A.set(node1,node1, A.get(node1,node1)+ G);
			A.set(node1, node1, A.get(node1, node1) + Gc);

			if  (node2 != 0)
			{
			A.set(node1,node2, A.get(node1,node2) - G);
			A.set(node2,node1, A.get(node2,node1) - G);
			A.set(node2,node2, A.get(node2,node2)+ G);
			A.set(node2, node2, A.get(node2, node2) + Gc);

			}
			}
			else if (node2 != 0)
			{
			A.set(node2,node2, A.get(node2,node2)+ G);
			A.set(node2, node2, A.get(node2, node2) + Gc);
			}
		};
		
void PILine::Init(ComplexMatrix& A, ComplexMatrix& B){
applyMatrixStamp(A,B);
}


void PILine::Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol){
}


