#ifndef NONIDEALSOURCE_H
#define NONIDEALSOURCE_H
#include "CircuitElement.h"
#include "Matrix.h"
#include "ComplexMatrix.h"

#include <iostream>
using std::cout;
using std::endl;

class NonIdealSource : public CircuitElement {
	protected:
		double ActivePower;
		double NominalVoltage;
		Complex InternalImpedance;
		Complex Vcur, Vold;
		double delta;
		Complex G;
		double alfa;
		
	public:
		NonIdealSource() {;};
		NonIdealSource(std::string name, int src, int dest, double vnom, double Pnom, double R, double X, double alfa);
		
		void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) ;
		void Init(ComplexMatrix& A, ComplexMatrix& B);		
		void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);

};
#endif