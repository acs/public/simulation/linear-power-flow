#ifndef PILINE_H
#define PILINE_H
#include "CircuitElement.h"
#include "Matrix.h"
#include "ComplexMatrix.h"
#include <iostream>
using std::cout;
using std::endl;

class PILine : public CircuitElement {
	protected:
		double inductance;
		double resistance;
		Complex Z;
		Complex G;
		Complex Gc;

	public:
		PILine() {;};
		PILine(std::string name, int src, int dest, double R, double X, double Xc);
		void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B);
		void Init(ComplexMatrix& A, ComplexMatrix& B);
		void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);
	};
#endif