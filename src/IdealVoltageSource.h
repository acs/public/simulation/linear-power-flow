#ifndef IDEALVOLTAGESOURCE_H
#define IDEALVOLTAGESOURCE_H
#include "CircuitElement.h"
#include "Matrix.h"
#include "ComplexMatrix.h"

#include <iostream>
using std::cout;
using std::endl;

class IdealVoltageSource : public CircuitElement {
	protected:
		Complex SourceVoltage;
		int internalNodeNum;
		
	public:
		IdealVoltageSource() {;};
		IdealVoltageSource(std::string name, int src, int dest, double voltage, double phase);
		
		void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) ;
		void Init(ComplexMatrix& A, ComplexMatrix& B);		
		void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);

};
#endif