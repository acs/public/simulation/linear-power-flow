#ifndef CIRCUITELEMENT_H
#define CIRCUITELEMENT_H
#include "Matrix.h"
#include <string.h>
#include <iostream>
#include "ComplexProblem.h"
using std::cout;
using std::endl;

class CircuitElement {
	public:	
		CircuitElement();
		~CircuitElement();
		const char* getType();
		virtual void applyMatrixStamp(ComplexMatrix& A, ComplexMatrix& B) ;
		virtual void Init(ComplexMatrix& A, ComplexMatrix& B);
		virtual void Step(ComplexMatrix& A, ComplexMatrix& B, ComplexMatrix& sol);
		int getMaxNode();

protected:
		int node1; 
		int node2;
		std::string name;
};
#endif