#include "stdafx.h"
#include "LPF_Sim.h"

LPF_Sim::LPF_Sim(int nodes) {
		numNodes = nodes+1;
		P = ComplexProblem(numNodes);
		sol = ComplexMatrix(numNodes,1);
		oldsol = ComplexMatrix(numNodes,1);
};


LPF_Sim::~LPF_Sim() {
		while (!elements.isEmpty()) {
			CircuitElement* ptrToFree = elements.elementAt(0);
			delete ptrToFree;
			elements.remove(0);
		}
	};


int LPF_Sim::Init() {
	
		//create the matrices...
		for (int i = 0; i < elements.size(); i++) {
			elements.elementAt(i)->Init(P.A, P.B);
		}
				
		//A.printMatrix();
		P.PrepareSolution();
		return(1);
	};
	

void LPF_Sim::AddModelToList(CircuitElement *m)
{
	elements.add(m);
}

	
int LPF_Sim::Solve(ComplexMatrix Vini, int maxn, double toll, double alfa)
{
	int k = 1;
	int iter = 0;
	double err = 1e4;
	ComplexMatrix delta(numNodes,1);
	oldsol = Vini;
	while((iter <= maxn)&&(err >=toll)) {
		P.B.clear();
	
		for (int i = 0; i < elements.size(); i++) {
			elements.elementAt(i)->Step(P.A, P.B, oldsol);
		}

		sol = P.Solve();
		sol.printMatrix();
		delta = sol - oldsol;
		
		delta.printMatrix();

		err = delta.norm();
		//cout << err;
		oldsol = sol;
		iter++;
	}
	if (iter >= maxn)
		return(1);
	else
		return(0);
};




