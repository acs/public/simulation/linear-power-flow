#include "stdafx.h"
#include<iostream>
#include "complex.h"

Complex::Complex() {
	r = i = 0;
}

Complex::Complex(double re, double im) {
	r = re;
	i = im;
}

Complex::Complex(double re) {
	r = re;
	i = 0;
}


Complex Complex::operator +(Complex op2) {
	Complex a;
	a.r = r + op2.r;
	a.i = i + op2.i;
	return a;
}

Complex Complex::operator -(Complex op2) {
	Complex a;
	a.r = r - op2.r;
	a.i = i - op2.i;
	return a;
}

Complex Complex::operator *(Complex op2) {
	Complex a;
	a.r = r*op2.r-i*op2.i;
	a.i = i*op2.r+r*op2.i;
	return a;
}

Complex Complex::operator /(Complex op2) {
	Complex a;
	double den;
	den = op2.r*op2.r + op2.i*op2.i;
	a.r = (r*op2.r + i*op2.i)/den;
	a.i = (i*op2.r - r*op2.i)/den;
	return a;
}

void Complex::operator +=(Complex op2) {
	r += op2.r;
	i += op2.i;
}

void Complex::operator -=(Complex op2) {
	r -= op2.r;
	i -= op2.i;
}

void Complex::print() {
	if (i >= 0)
		cout << r << "+j" << i;
	else
		cout << r << "-j" << fabs(i);
}

Complex Complex::Conj()
{
	Complex out;
	out.r = r;
	out.i = -i;
	return out;
}

double Complex::abs()
{
	return sqrt(r*r+i*i);
}

double Complex::arg()
{
	return atan2(r, i);
}