#include "stdafx.h"
#include "TopologyReaderLPF.h"


TopologyReaderLPF::TopologyReaderLPF() {

}

TopologyReaderLPF::~TopologyReaderLPF() {

}

/// Parse topology file
/// @param f Openend stream containing the configuration file.
/// @param conf Configuration structure that is filled out during parsing.
/// @return 0 if successfull, nonzero otherwise.

int TopologyReaderLPF::readConfig(std::ifstream &f, Config &conf) {
	std::string s;
		
	getline(f, s);

	if (conf.MaxIter.empty()) {
		std::string::size_type p = s.find(',');
		conf.MaxIter = s.substr(0, p);
		s.erase(0, p + 1);
	}

	if (conf.Toll.empty()) {
		std::string::size_type p = s.find(',');
		conf.Toll = s.substr(0, p);
		s.erase(0, p + 1);
	}

	if (conf.Alfa.empty()) {
		std::string::size_type p = s.find(',');
		conf.Alfa = s.substr(0, p);
		s.erase(0, p + 1);
	}

	for (int line = 1; f.good(); line++) {
		
		getline(f, s);

		if (!s.empty() && s[0] == '#') 			
			continue;

		Element element;

		for (int elementIdx = 1; s.find(',') != std::string::npos; elementIdx++) {
			if (element.type.empty()) {
				std::string::size_type p = s.find(',');				
				element.type = s.substr(0, p);
				s.erase(0, p + 1);
			}
			else if (element.name.empty()) {
				std::string::size_type p = s.find(',');			
				element.name = s.substr(0, p);
				s.erase(0, p + 1);
			}
			else if (element.node1.empty()) {
				std::string::size_type p = s.find(',');			
				element.node1 = s.substr(0, p);
				s.erase(0, p + 1);
			}
			else if (element.node2.empty()) {
				std::string::size_type p = s.find(',');			
				element.node2 = s.substr(0, p);
				s.erase(0, p + 1);
			}
			else {
				std::string::size_type p = s.find(',');		
				element.parameters.push_back(s.substr(0, p));
				s.erase(0, p + 1);
			}
		}

		conf.elements.push_back(element);
	}
	return 0;
}


/// Use the parsed configuration structure to create circuit elements

int TopologyReaderLPF::parseConfig(Config &conf, vector<CircuitElement*> &circElements, int &MaxIter, double &Toll, double &Alfa) {

	MaxIter = std::stoi(conf.MaxIter);
	Toll = std::stod(conf.Toll);
	Alfa = std::stod(conf.Alfa);

	for (std::vector<Element>::iterator it = conf.elements.begin(); it != conf.elements.end(); ++it) {
		
		CircuitElement* tmpCircElement;

		if (it->type.compare("IdealVoltageSource") == 0) {
			tmpCircElement = new IdealVoltageSource(it->name, std::stoi(it->node1), std::stoi(it->node2), std::stod(it->parameters[0]), std::stod(it->parameters[1]));
		}
		else if (it->type.compare("Impedance") == 0) {
			tmpCircElement = new Impedance(it->name, std::stoi(it->node1), std::stoi(it->node2), std::stod(it->parameters[0]), std::stod(it->parameters[1]));
		}
		else if (it->type.compare("ConstantPowerLoad") == 0) {
			tmpCircElement = new ConstantPowerLoad(it->name, std::stoi(it->node1), std::stoi(it->node2), std::stod(it->parameters[0]), std::stod(it->parameters[1]));
		}
		else if (it->type.compare("PILine") == 0) {
			tmpCircElement = new PILine(it->name, std::stoi(it->node1), std::stoi(it->node2), std::stod(it->parameters[0]), std::stod(it->parameters[1]), std::stod(it->parameters[2]));
		}
		else if (it->type.compare("PILine") == 0) {
			tmpCircElement = new NonIdealSource(it->name, std::stoi(it->node1), std::stoi(it->node2), std::stod(it->parameters[0]), std::stod(it->parameters[1]), std::stod(it->parameters[2]), std::stod(it->parameters[3]), std::stod(it->parameters[4]));
		}
		else {

		}
		circElements.push_back(tmpCircElement);
	}

	return 0;
}