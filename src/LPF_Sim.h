#ifndef LPF_SIM_H
#define LPF_SIM_H
//#include "LU.h"
#include "mylibrary.h"
#include <iostream>
#include "complex.h"
#include "ComplexProblem.h"

using std::cout;
using std::endl;

class LPF_Sim {
protected:
	VectorJI<CircuitElement *> elements;
	int numNodes;	

public:
	ComplexProblem P;
	ComplexMatrix sol;
	ComplexMatrix oldsol;

	LPF_Sim(int nodes);
	
	~LPF_Sim();
	
	void AddModelToList(CircuitElement *m);

	int Init();
	int Solve(ComplexMatrix Vini, int maxn, double toll, double alfa);

};
#endif






